<?php

namespace App\Http\Controllers;

use Infrastructure\Abstracts\ControllerAbstract;

class AuthController extends ControllerAbstract
{
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function forgotPassword()
    {
        //todo::
    }

    public function resetPassword()
    {
        //todo::
    }
}
