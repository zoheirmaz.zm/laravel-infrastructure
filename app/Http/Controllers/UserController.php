<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use Infrastructure\Abstracts\ControllerAbstract;
use Infrastructure\Exceptions\AuthorizationException;
use Infrastructure\Interfaces\Repositories\UserRepositoryInterface;

class UserController extends ControllerAbstract
{
    protected function me(Request $request)
    {
        return new UserResource(Auth::user());
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return array
     * @throws AuthorizationException
     */
    public function login(Request $request)
    {
        $credentials = $request->input();

        if (!$token = auth()->attempt($credentials)) {
            throw new AuthorizationException();
        }

        return $this->respondWithToken($token);
    }

    public function register(Request $request)
    {
        $credentials = $request->input();

        /** @var UserRepositoryInterface $repository */
        $repository = app(UserRepositoryInterface::class);
        $user = $repository->create($credentials);

        $token = auth()->login($user);

        return $this->respondWithToken($token);
    }

    public function logout(Request $request)
    {
        auth()->logout();

        return ['message' => 'Successfully logged out'];
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return array
     */
    private function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }
}
