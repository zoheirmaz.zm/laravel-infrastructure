<?php

namespace App\Policies;

use Infrastructure\Abstracts\PolicyAbstract;

class User extends PolicyAbstract
{
    public function me()
    {
        $this->userMustBeAuthenticated();

        return true;
    }

    public function login()
    {
        return true;
    }

    public function register()
    {
        return true;
    }

    public function logout()
    {
        $this->userMustBeAuthenticated();

        return true;
    }
}
