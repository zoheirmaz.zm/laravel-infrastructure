<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        $this->checkToEnableQueryLog();
    }

    private function checkToEnableQueryLog()
    {
        if (app()->environment() == 'development') {
            DB::connection()->enableQueryLog();

            $GLOBALS['queryCount'] = 0;
            DB::listen(function ($query) {
                $GLOBALS['queryCount']++;
            });
        }
    }
}
