<?php

namespace App\Providers;

use App\Repositories as Repositories;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Interfaces\Repositories as RepositoryInterfaces;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(
            RepositoryInterfaces\UserRepositoryInterface::class,
            Repositories\UserRepository::class
        );
    }
}
