<?php

namespace App\Repositories;

use App\Entities\User;
use Illuminate\Support\Facades\Hash;
use Infrastructure\Traits\QueryTrait;
use Infrastructure\Traits\EntityRelationsTrait;
use Infrastructure\Interfaces\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    use EntityRelationsTrait, QueryTrait;

    public function create($data)
    {
        return User::create([
            'name' => $data['name'],
            'password' => Hash::make($data['password']),
            'mobile' => $data['mobile'],
        ]);
    }
}
