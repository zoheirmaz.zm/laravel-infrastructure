<?php

namespace App\Validations\User;

use Infrastructure\Abstracts\ValidationAbstract;

class Login extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'mobile' => [
                'required',
                'string',
//                'mobile',
            ],
            'password' => [
                'required',
                'string',
            ],
            'email' => [
                'email',
            ],
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
