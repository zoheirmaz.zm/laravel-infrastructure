<?php

namespace App\Validations\User;

use Infrastructure\Abstracts\ValidationAbstract;

class Logout extends ValidationAbstract
{
    public function rules(): array
    {
        return [];
    }

    public function messages(): array
    {
        return [];
    }
}
