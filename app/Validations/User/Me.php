<?php

namespace App\Validations\User;

use Infrastructure\Abstracts\ValidationAbstract;

class Me extends ValidationAbstract
{
    public function rules(): array
    {
        return [];
    }

    public function messages(): array
    {
        return [];
    }
}
