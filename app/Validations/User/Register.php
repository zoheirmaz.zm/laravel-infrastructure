<?php

namespace App\Validations\User;

use Infrastructure\Abstracts\ValidationAbstract;

class Register extends ValidationAbstract
{
    public function rules(): array
    {
        return [
            'mobile' => [
                'required',
                'string',
            ],
            'password' => [
                'required',
                'string',
            ],
            'name' => [
                'required',
                'string',
            ],
        ];
    }

    public function messages(): array
    {
        return [];
    }
}
