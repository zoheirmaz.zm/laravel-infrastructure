<?php

namespace Infrastructure\Abstracts;

use Illuminate\Support\Facades\Auth;
use Infrastructure\Exceptions\AuthenticationException;

abstract class PolicyAbstract
{
    public $message = null;

    /**
     * @throws AuthenticationException
     */
    protected function userMustBeAuthenticated()
    {
        if (!Auth::check()) {
            throw new AuthenticationException('user must be authenticated');
        }
    }

    public function getMessage()
    {
        return $this->message;
    }
}
