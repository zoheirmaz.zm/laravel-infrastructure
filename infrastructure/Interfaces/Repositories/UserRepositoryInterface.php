<?php

namespace Infrastructure\Interfaces\Repositories;

use App\Entities\User;

interface UserRepositoryInterface
{
    /**
     * @param array $data
     * @return User
     */
    public function create($data);
}
