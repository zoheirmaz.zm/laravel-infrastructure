<?php

namespace Infrastructure\Traits;

use Illuminate\Http\Response;
use Infrastructure\Enums\ExceptionEnums;
use Infrastructure\Abstracts\ExceptionAbstract;

trait ExceptionTrait
{
    protected function validationRender(ExceptionAbstract $exception)
    {
        return response()->json([
            'error' => [
                'type' => $exception->getCategory(),
                'messages' => $this->getValidationMessage($exception->getMessage())
            ]
        ], $exception->getCode());
    }

    protected function authorizationRender(ExceptionAbstract $exception)
    {
        return response()->json([
            'error' => [
                'type' => $exception->getCategory(),
                'messages' => [
                    [
                        'text' => $exception->getMessage()
                    ]
                ]
            ]
        ], $exception->getCode());
    }

    protected function authenticationRender(ExceptionAbstract $exception)
    {
        return response()->json([
            'error' => [
                'type' => $exception->getCategory(),
                'messages' => [
                    [
                        'text' => $exception->getMessage()
                    ]
                ]
            ]
        ], $exception->getCode());
    }


    protected function internalRender($request, $exception)
    {
        return response()->json([
            'error' => [
                'type' => ExceptionEnums::INTERNAL,
                'messages' => [
                    [
                        'text' => $exception->getMessage()
                    ]
                ],
            ]
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    protected function logHeaderRender(ExceptionAbstract $exception)
    {
        return response()->json([
            'error' => [
                'type' => $exception->getCategory(),
                'messages' => [
                    [
                        'text' => $exception->getMessage()
                    ]
                ],
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString()
            ]
        ], $exception->getCode());
    }

    protected function logHeaderOtherExceptionRender(\Throwable $exception)
    {
        return response()->json([
            'error' => [
                'messages' => [
                    [
                        'text' => $exception->getMessage()
                    ]
                ],
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'trace' => $exception->getTraceAsString()
            ]
        ], 500);
    }

    private function getValidationMessage($errors)
    {
        $data = [];
        $index = 0;

        foreach (json_decode($errors) as $key => $error) {
            $data[$index] = [];
            $error = collect($error);
            $data[$index]['title'] = $key;
            $data[$index]['text'] = $error->values()[0];
            $index++;
        };

        return $data;
    }

}
