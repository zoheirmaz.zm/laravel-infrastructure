<?php

namespace Infrastructure\Traits;

use Illuminate\Database\Query\Builder;

trait QueryTrait
{
    /** @return Builder */
    public function appendSizeFilterQuery($query, $filter)
    {
        return $query
            ->offset($filter['offset'] ?? 0)
            ->limit($filter['size'] ?? 20);
    }

    /** @return Builder */
    public function sortQuery($query, $sort)
    {
        return $query->orderBy($sort['item'], $sort['type']);
    }
}
